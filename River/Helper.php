<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 0:47
 */ 
class Helper
{
    public static function getMembersFlat(array $members)
    {
        $result = array();
        // @todo use array function
        foreach ($members['adults'] as $member) {
            $result[] = $member;
        }
        foreach ($members['children'] as $member) {
            $result[] = $member;
        }

        return $result;
    }
}
