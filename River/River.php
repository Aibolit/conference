<?php
/**
 * http://pythagor.com
 * Date: 03.12.14
 * Time: 22:38
 */

namespace pythagor\conference;

use Helper;

class River implements BarrierInterface
{
    const STATE_HERE = 1;
    const STATE_THERE = 2;
    private $_state = 1;

    private $_sideOne = array(
        'adults' => array(),
        'children' => array(),
    );
    private $_sideTwo = array(
        'adults' => array(),
        'children' => array(),
    );

    public function traverse(Family $family)
    {
        Logger::info('Start traversing');
        $members = $family->getMembers();
        if (count($members['children']) < 2) {
            throw new RiverException('Mission Impossible!');
        }
        $this->_sideOne = $members;

        while (count(Helper::getMembersFlat($this->_sideOne)) > 0) {
            if ($this->_state === self::STATE_HERE) {
                $this->thereto();
            }

            if ($this->_state === self::STATE_THERE && count(Helper::getMembersFlat($this->_sideOne)) > 0) {
                $this->back();
            }
            sleep(1);
        }
        Logger::info('Traversing over');
    }

    private function thereto()
    {
        Logger::info('Im here. Im going to there.');
        $members = $this->selectMembersHere();
        foreach ($members as $member) {
            if ($member instanceof Adult) {
                $this->_sideTwo['adults'][] = $member;
            } elseif ($member instanceof Child) {
                $this->_sideTwo['children'][] = $member;
            }
        }
        $this->_state = self::STATE_THERE;
    }

    private function back()
    {
        Logger::info('Im there. Im going to back.');
        $member = $this->selectMemberThere();
        if ($member instanceof Adult) {
            $this->_sideOne['adults'][] = $member;
        } elseif ($member instanceof Child) {
            $this->_sideOne['children'][] = $member;
        }
        $this->_state = self::STATE_HERE;
    }

    private function selectMembersHere()
    {
        $selectedMembers = array();

        if (count($this->_sideTwo['children']) > 0) {
            if (count($this->_sideOne['adults']) > 0) {
                $selectedMembers[] = array_shift($this->_sideOne['adults']);
            } else {
                while (count($this->_sideOne['children']) > 0 && count($selectedMembers) < 2) {
                    $selectedMembers[] = array_shift($this->_sideOne['children']);
                }
            }
        } else {
            while (count($this->_sideOne['children']) > 0 && count($selectedMembers) < 2) {
                $selectedMembers[] = array_shift($this->_sideOne['children']);
            }
        }

        Logger::info('Picked up forward: ', $selectedMembers);
        return $selectedMembers;
    }

    private function selectMemberThere()
    {
        $selectedMember = null;
        if (count($this->_sideTwo['children']) > 0) {
            $selectedMember = array_shift($this->_sideTwo['children']);
        } else {
            throw new RiverException('Mission impossible!');
        }

        Logger::info('Picked up backward: ', array($selectedMember));
        return $selectedMember;
    }
}
