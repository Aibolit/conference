<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 15:45
 */

namespace pythagor\conference;


interface LoggerInterface
{
    public function write($message);
} 
