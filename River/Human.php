<?php
/**
 * http://pythagor.com
 * Date: 03.12.14
 * Time: 21:46
 */

namespace pythagor\conference;

abstract class Human
{
    const SEX_MALE = 'male';
    const SEX_FEMALE = 'female';

    private $age;

    private $name;

    private $sex;

    private $options = array();

    public function __construct($name, $age, $sex, $options = array())
    {
        $this->name = $name;
        $this->age = $age;
        $this->sex = $sex;
        $this->options = $options;
    }

    public function __toString()
    {
        return $this->name;
    }
} 
