<?php
/**
 * http://pythagor.com
 * Date: 03.12.14
 * Time: 21:52
 */

namespace pythagor\conference;

// Simple Init
spl_autoload_register(function ($class) {
    $className = str_replace(__NAMESPACE__ . '\\', '', $class);
    include $className . '.php';
});

$fileLogger = new FileLogger('log.txt');
Logger::setLogger($fileLogger);

// Using

$family = new Family();

$family->addMember(new Adult('Boris', 45, Human::SEX_MALE));
$family->addMember(new Adult('Maria', 40, Human::SEX_FEMALE));
$family->addMember(new Child('Vitya', 12, Human::SEX_MALE));
$family->addMember(new Child('Masha', 10, Human::SEX_FEMALE));
$family->addMember(new Child('Sasha', 14, Human::SEX_MALE));
$family->addMember(new Adult('Fisherman Petrovich', 60, Human::SEX_MALE));

$river = new River();
$river->traverse($family);
