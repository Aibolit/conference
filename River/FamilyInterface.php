<?php
/**
 * http://pythagor.com
 * Date: 03.12.14
 * Time: 21:48
 */

namespace pythagor\conference;


interface FamilyInterface
{
    public function addMember(Human $member);

    public function getMembers();
}
