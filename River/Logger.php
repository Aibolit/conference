<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 15:20
 */

namespace pythagor\conference;


class Logger
{
    private static $instance;

    public static function setLogger(LoggerInterface $instance)
    {
        self::$instance = $instance;
    }

    private function log($level, $message, array $data = array())
    {
        if (self::$instance instanceof LoggerInterface) {
            $logger = self::$instance;
        } else {
            throw new \Exception('Logger is not configured.');
        }
        if (!empty($data)) {
            $dataMessage = self::prepareData($data);
            $message .= $dataMessage;
        }
        $message = time() . ' - ' . $level . ' - ' . $message;

        $logger->write($message);
    }

    public static function error($message, array $data = array())
    {
        self::log('error', $message, $data);
    }

    public static function info($message, array $data = array())
    {
        self::log('info', $message, $data);
    }

    private static function prepareData($data)
    {
        return implode(', ', $data);
    }
}
