<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 15:45
 */

namespace pythagor\conference;


class FileLogger implements LoggerInterface
{
    private $file;

    public function __construct($file)
    {
        if (!file_exists($file)) {
            $handler = fopen($file, 'a');
            fclose($handler);
        }
        $this->file = $file;
    }

    public function write($message)
    {
        $message = $message . PHP_EOL;
        echo $message;
        $handler = fopen($this->file, 'a');
        fwrite($handler, $message);
        fclose($handler);
    }

} 
