<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 15:11
 */

namespace pythagor\conference;


interface BarrierInterface
{
    public function traverse(Family $family);
}
