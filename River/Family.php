<?php
/**
 * http://pythagor.com
 * Date: 03.12.14
 * Time: 21:55
 */

namespace pythagor\conference;

use Helper;

class Family implements FamilyInterface
{
    private $_members = array(
        'adults'   => array(),
        'children' => array(),
    );

    public function addMember(Human $member)
    {
        if ($member instanceof Adult) {
            $this->_members['adults'][] = $member;
        }
        if ($member instanceof Child) {
            $this->_members['children'][] = $member;
        }

    }

    public function getMembers()
    {
        return $this->_members;
    }
}
