<?php
/**
 * http://pythagor.com
 * Date: 04.12.14
 * Time: 16:10
 */

namespace pythagor\conference;


class RiverException extends \Exception
{
    public function __construct($message = '', $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        Logger::error($message . PHP_EOL);
    }
} 
